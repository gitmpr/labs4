#!/bin/bash
 
if [[ $(id -u) -ne 0 ]]; then
    echo "Not running as root. Abort!"
    exit 1
else
    echo "Running as root. Continuing.."

    case $1 in
        create)
            echo "creating.."
            read -p "How many? " USERCOUNT
            for (( i = 1; i <= $USERCOUNT; i++ ))
            do
                useradd -m user${i}
                cat << END > /home/user${i}/welcome.txt
Welcome new user.
Here is a welcoming text file
on multiple lines
don't misbehave
END
            done
            ;;
        remove)
            echo "removing.."
            read -p "How many? " USERCOUNT
            for (( i = 1; i <= $USERCOUNT; i++ ))
            do
                userdel user${i}
            done
            ;;
        *)
            echo "error: operator not recognized"
            ;;
    esac
fi

